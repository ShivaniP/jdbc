package com.lms.exception;

public class LaundryNumNotFoundException extends Exception {
	
	public LaundryNumNotFoundException(String msg) {
		super(msg);
	}
}
