package com.lms.dao;

import java.sql.Connection;



import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.sql.Date;
import java.util.List;

import com.lms.model.Laundry;
import com.lms.util.DbConnection;


public class LaundryDAOImpl implements LaundryDAO {
	
	Laundry lDAO;
	public LaundryDAOImpl() {
		
	}
	
	public boolean addLaundry(Laundry Laundry) throws SQLException,ClassNotFoundException {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = con.prepareStatement("insert into LaundryTable(OrderId,noOfShirts,norOfTrousers,noOfSkirts,noOfJeans) values(?,?,?,?,?)");
		pst.setInt(5, Laundry.getOrderId());
		pst.setInt(1, Laundry.getnoOfShirts());
		pst.setInt(2, Laundry.getnoOfTrousers());
		pst.setInt(3, Laundry.getnoOfSkirts());
		pst.setInt(4, Laundry.getnoOfJeans());
		if (pst.execute()) {
			return true;
		}
		return false;
	}
	public List<Laundry> listAllOrders() throws SQLException,ClassNotFoundException{
		List<Laundry> updateLaundryListByContractDate =  updateLaundryByContractDate();
		List<Laundry> deleteLaundryListByContractDate =  deleteLaundryByContractDate(updateLaundryListByContractDate);
		return deleteLaundryListByContractDate;
		
	}
	public boolean deleteOrder(int LaundryId) throws SQLException,ClassNotFoundException {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = con.prepareStatement("delete FROM LaundryTable where OrderId=?");
		pst.setInt(1,LaundryId);
        return !(pst.execute());
        
	}
	public boolean updateOrder(Laundry Laundry) throws SQLException,ClassNotFoundException {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = con.prepareStatement("update LaundryTable set noOfShirts=?,noOfTrousers=?,noOfSkirts=?,noOfJeans=?  where OrderId=?");
		//pst.setInt(6, Laundry.empId());
		pst.setInt(1, Laundry.getOrderId());
		pst.setInt(2, Laundry.getnoOfShirts());
		pst.setInt(3, Laundry.getnoOfTrousers());
		pst.setInt(4, Laundry.getnoOfSkirts());
		pst.setInt(5, Laundry.getnoOfJeans());
		
		return true;
	}
	
	

}
