package com.lms.dao;

import java.sql.SQLException;

import java.util.List;
import com.lms.model.Laundry;

public interface LaundryDAO {
	
	public boolean addLaundry(Laundry laundry) throws SQLException,ClassNotFoundException;
	public List<Laundry> listAllOrders() throws SQLException,ClassNotFoundException;
	public boolean deleteOrder(int OrderId) throws SQLException,ClassNotFoundException;
	public boolean updateOrder(Laundry OrderId) throws SQLException,ClassNotFoundException;


}
