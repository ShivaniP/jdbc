package com.lms.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lms.model.Laundry;
import com.lms.model.Laundry;
import com.lms.service.LaundryService;
import com.lms.service.LaundryServiceImpl;


/**
 * Servlet implementation class UpdateProductController
 */
@WebServlet("/UpdateLaundry")
public class UpdateLaundry extends HttpServlet {
	//private static final long serialVersionUID = 1L;
       
   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		Integer OrderId=Integer.parseInt(request.getParameter("OrderId"));
		Integer noOfShirts=Integer.parseInt(request.getParameter("noOfShirts"));
		Integer noOfTrousers = Integer.parseInt(request.getParameter("noOfTrousers"));
		Integer noOfJeans = Integer.parseInt(request.getParameter("noOfJeans"));
		Integer noOfSkirts = Integer.parseInt(request.getParameter("noOfSkirts"));
		//LocalDate contractendDate = null;
		Laundry Laundry = new Laundry();
		Laundry.setOrderId(OrderId);
		Laundry.setnoOfShirts(noOfShirts);
		Laundry.setnoOfTrousers(noOfTrousers);
		Laundry.setnoOfJeans(noOfJeans);
		Laundry.setnoOfSkirts(noOfSkirts);
		try {
			LaundryService ps = new LaundryServiceImpl();
			boolean result = ps.updateOrder(Laundry);
			if(result) {
				out.println("Laundry updated");
			}
			else {
				out.println("not updated");
			}
		
	}catch(ClassNotFoundException e) {
		e.printStackTrace();
	}catch(SQLException e) {
		e.printStackTrace();
	}
		

}}
