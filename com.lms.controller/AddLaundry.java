package com.lms.controller;

import java.io.IOException;


import java.io.PrintWriter;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lms.model.Laundry;
import com.lms.service.LaundryService;
import com.lms.service.LaundryServiceImpl;

/**
 * Servlet implementation class ProductController
 */
@WebServlet("/AddLaundry")
public class AddLaundry extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		int OrderId=request.getParameter("OrderId");
		int noOfShirts=request.getParameter("noOfShirts");
		int noOfTrousers = request.getParameter("noOfTrousers");
		int noOfJeans = request.getParameter("noOfJeans"));
		int noOfSkirts = Integer.parseInt(request.getParameter("noOfSkirts"));
		Laundry Laundry = new Laundry();
		Laundry.setOrderId(OrderId);
		Laundry.setnoOfShirts(noOfShirts);
		Laundry.setnoOfTrousers(noOfTrousers);
		Laundry.setnoOfSkirts(noOfJeans);
		Laundry.setnoOfJeans(noOfSkirts);
		try {
			LaundryService es = new LaundryServiceImpl();
			boolean result = es.addLaundryOrder(Laundry);
			if(result) {
				out.println("Laundry added");
			}
			else {
				out.println("not added");
			}
		
	}catch(ClassNotFoundException e) {
		e.printStackTrace();
	}catch(SQLException e) {
		e.printStackTrace();
	}
		

}}