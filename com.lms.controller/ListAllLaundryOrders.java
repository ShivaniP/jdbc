package com.lms.controller;

import java.io.IOException;

import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lms.model.Laundry;
import com.lms.service.LaundryService;
import com.lms.service.LaundryServiceImpl;

/**
 * Servlet implementation class GetProductController
 */
@WebServlet("/ListAllLaundrys")
public class ListAllLaundryOrders extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out= response.getWriter();
		response.setContentType("text/html");
		List<Laundry> llist = new ArrayList<Laundry>();
		LaundryService ls = new LaundryServiceImpl();
		out.println("<table align = 'center' border='2'>");
		try {
			llist = ls.listAllOrders();
			for(Laundry l:llist) {
				out.println("<tr><td>"+l.getOrderId()+"</td><td>"+l.getnoOfShirts()+"</td><td>"+l.getnoOfTrousers()+"</td><td>"+l.getnoOfSkirts()+"</td><td>"+l.getnoOfJeans()+"</td><td>"+"</tr>");
			}
			
		}catch(ClassNotFoundException e) {
			e.getMessage();
		}catch(SQLException e) {
			e.getMessage();
		}
		out.println("</table>");
	}

}
