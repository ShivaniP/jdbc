package com.lms.controller;

import java.io.IOException;

import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lms.model.Laundry;
import com.lms.service.LaundryService;
import com.lms.service.LaundryServiceImpl;



/**
 * Servlet implementation class DeleteProductController
 */
@WebServlet("/DeleteLaundry")
public class DeleteLaundry extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		Integer id=request.getParameter("id");
	    //Laundry Laundry = new Laundry();
		try {
			LaundryService ls = new LaundryServiceImpl();
			boolean result = ls.deleteOrder(id);
			if(result) {
				out.println("Product deleted");
			}
			else {
				out.println("Product not deleted");
			}
		
	}catch(ClassNotFoundException e) {
		e.printStackTrace();
	}catch(SQLException e) {
		e.printStackTrace();
	}
		

}}