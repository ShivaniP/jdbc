package com.lms.service;

import java.sql.SQLException;



import java.util.ArrayList;
import java.util.List;


import com.lms.dao.LaundryDAO;
import com.lms.dao.LaundryDAOImpl;
import com.lms.exception.LaundryNumNotFoundException;
import com.lms.model.Laundry;

public class LaundryServiceImpl implements LaundryService{
	LaundryDAO lDAO;
	
	public boolean addLaundry(Laundry Laundry) throws SQLException,ClassNotFoundException {
		// TODO Auto-generated method stub
		lDAO = new LaundryDAOImpl();
		boolean result = lDAO.addLaundry(Laundry);
		return result;
	}

	
	public List<Laundry> listAllOrders() throws SQLException,ClassNotFoundException {
		
		List<Laundry> plist=new ArrayList<Laundry>();
		lDAO = new LaundryDAOImpl();
		plist = lDAO.listAllLaundrys();
		try{
			if(plist.size() == 0) {
				throw new LaundryListEmptyException("List is Empty");
			}
		}catch(Exception e) {
			System.out.println(e.getMessage());
		}
		return plist;
	}
	public boolean deleteOrder(int OrderId) throws SQLException,ClassNotFoundException {
		
		lDAO = new LaundryDAOImpl();
		boolean result = lDAO.deleteLaundry(OrderId);
		return result;
	}
	public boolean updateOrder(Laundry Laundry) throws SQLException,ClassNotFoundException {
		// TODO Auto-generated method stub
		lDAO = new LaundryDAOImpl();
		boolean result = lDAO.updateLaundry(Laundry);
		return result;
	}


	
}