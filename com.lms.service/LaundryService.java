package com.lms.service;

import java.sql.SQLException;


import java.util.List;

import com.lms.model.Laundry;

public interface LaundryService {
	public boolean addLaundryOrder(Laundry laundry) throws SQLException,ClassNotFoundException;
	public List<Laundry> listAllOrders() throws SQLException,ClassNotFoundException;
	public boolean deleteOrder(int OrderId) throws SQLException,ClassNotFoundException;
	public boolean updateOrder(Laundry laundry) throws SQLException,ClassNotFoundException;


}