package com.lms.model;

import java.sql.Date;
import java.time.LocalDate;
import java.util.*;
public class Laundry {
	private int OrderId;
	private int noOfShirts;
	private int noOfTrousers;
	private int noOfSkirts;
	private int noOfJeans;
	
	public Laundry() {
		
	}

	public Laundry(int noOfShirts,int OrderId,int noOfTrousers, int noOfSkirts, int noOfJeans) {
		//super();
		this.OrderId = OrderId;
		this.noOfShirts = noOfShirts;
		this.noOfTrousers = noOfTrousers;
		this.noOfSkirts = noOfSkirts;
		this.noOfJeans = noOfJeans;
	}

	public int getnoOfShirts() {
		return noOfShirts;
	}

	public void setnoOfShirts(int noOfShirts) {
		this.noOfShirts = noOfShirts;
	}

	public int getnoOfTrousers() {
		return noOfTrousers;
	}

	public void setnoOfTrousers(int noOfTrousers) {
		this.noOfTrousers = noOfTrousers;
	}

	public int getnoOfSkirts() {
		return noOfSkirts;
	}

	public void setnoOfSkirts(int noOfSkirts) {
		this.noOfSkirts = noOfSkirts;
	}

	public int getnoOfJeans() {
		return noOfJeans;
	}

	public void setnoOfJeans(int noOfJeans) {
		this.noOfJeans = noOfJeans;
	}
	public int getOrderId() {
		return OrderId;
	}

	public void setOrderId(int OrderId) {
		this.OrderId = OrderId;
	}


	
	

}
